import os
import re
import csv
import random
import json
import names
import uuid
from itertools import combinations

class Migration:
  
  def __init__(self):
    super().__init__()
    self.company_id = "AIT"

  def standardized(self, raw_data):
    # Lọc bỏ các ký tự dư thừa, dấu câu, lỗi HTML, mã ký tự đặc biệt HTML(1)
    return re.sub('[^A-Za-z0-9]+', '', raw_data)

  def get_skills(self):
    res = []
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'system_skills.csv') 
    with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            # record = row[1]
              # "company": row[0],
              # "skill_id": row[1],
              # "skill_name": row[2],
              # "parent_id": row[3],
              # "category": row[4]
            record = {
              "skill_id": row[1],
              "skill_name": row[2],
            }
            res.append(record)
    self.skills = res
    return res

  def get_skill_relevancies(self):
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'edge.json') 
    with open(input_file_path, 'r', encoding='cp932', errors='ignore') as f:
      data = json.loads(f.read())
    return data["data"]

  def phuclq_system_skills(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'phuclq_system_skills.aql') 
      skills = self.get_skills()
      items = []
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        for skill in skills:
          # key = self.standardized(skill["skill_name"])
          item = {
            "_key": skill["skill_id"],
            "_id": skill["skill_id"],
            "skill_name": skill["skill_name"]
           }
          items.append(item)
        output_file.write("FOR doc in "+ json.dumps(items)+" INSERT doc IN system_skills")
    except Exception as e:
      print(e)


  def phuclq_skill_similarity_edges(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'phuclq_skill_similarity_edges.aql') 
      skill_relevancies = self.get_skill_relevancies()
      items = []
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        for skill in skill_relevancies:
          item = {
            "_from":"system_skills/{skill_id}".format(skill_id=skill["source_skill"]["skill_id"]),
            "_to":"system_skills/{skill_id}".format(skill_id=skill["target_skill"]["skill_id"]),
            "similarity":skill["relevant_value"],
            "_from_skill": skill["source_skill"]["skill_name"],
            "_to_skill": skill["target_skill"]["skill_name"],
          }
          items.append(item)
        output_file.write("FOR doc in "+ json.dumps(items)+" INSERT doc IN skill_similarity_edges")
    except Exception as e:
      print(e)

  def get_skills_for_relevancies(self):
    res = []
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'system_skills.csv') 
    with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            if row[4] == "hardskill":
              record = {
                # "company": row[0],
                "skill_id": row[1],
                "skill_name": row[2],
                # "parent_id": row[3],
                # "category": row[4]
              }
              res.append(record)
    self.skills = res
    return res

  def system_skills(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'system_skills.aql')
      skills = self.get_skills()
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        """ items = []
        for skill in self.skills:   
          # skill["type"]= random.choice(["accepted", "pending"])
          print(skill)
          items.append(skill) """
        output_file.write("FOR doc in "+ json.dumps(self.skills)+" INSERT doc IN system_skills")
        # output_file.write("insert " + json.dumps(items)+" into system_skill;" +"\n")
    except Exception as e:
      print(str(e))

  def transactions(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'transactions.aql')
      system_skills = self.get_skills()
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        items = []
        i = 0
        while(i<number):
          skill_number = int(round(random.uniform(7,10), 0))
          num = 0
          required_skills = []
          while(num<skill_number):
            skill = random.choice(system_skills)
            required_skills.append(skill)
            num+=1
            record = {
              "company_id": self.company_id,
              "id": str(uuid.uuid4()),
              "skill_id": skill["skill_id"],
              "skill_name": skill["skill_name"],
              "type": random.choice(["skill_matching", "employee_skill", "employee_project_history", "project_info", "other"])
            }
            items.append(record)   
          i+=1
        output_file.write("FOR doc in "+ json.dumps(items)+" INSERT doc IN transactions")
        # output_file.write("insert " + json.dumps(items)+" into system_skill;" +"\n")
    except Exception as e:
      print(str(e))

  def get_employees(self):
    res = []
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'employees.csv') 
    with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
      csv_reader = csv.reader(csv_file, delimiter=',')
      for row in csv_reader:
        record = {
          "user_id": row[0],
          "company": row[1],
          # "lang": row[2],
          # "dept_id": row[3],
          "id": row[4],
          # "first_name": row[5],
          # "last_name": row[6],
          # "emp_code": row[7]
        }
        res.append(record)
    self.employees = res
    return res

  def employee_skills(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'employees_skills.aql') 
      system_skills = self.get_skills()
      employees = self.get_employees()

      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        # n records

        for employee in employees:
          skill_number = int(round(random.uniform(3,5), 0))
          num = 0
          employee_skills = []
          while(num<skill_number):
            skill = random.choice(system_skills)
            if skill not in employee_skills:
              employee_skills.append(skill) 
            num+=1
          employee["employee_skills"]= employee_skills
          print(employee)
        # employees = json.loads(employees)

        output_file.write("FOR doc in "+ str(employees)+" INSERT doc IN employees")
    except Exception as e:
      print(e)

  def relevancies(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'skill_relevancies.aql') 
      skills = self.get_skills_for_relevancies()
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        items = []
        for skill in skills:
          item = {
            "company_id": "f1e21c68-c85b-4c91-bd82-2f742d6f7be9",
            "id": str(uuid.uuid4()),
            "source_skill": skill,
            "target_skill": skill,
            "relevant_value": 1
          }

          items.append(item)
        pairs = list(combinations(skills, 2))

        for pair in pairs:
          record = {
            "company_id": "f1e21c68-c85b-4c91-bd82-2f742d6f7be9",
            "id": str(uuid.uuid4()),
            "source_skill": pair[0],
            "target_skill": pair[1],
            "relevant_value": round(random.uniform(0,1), 7)
          }
          print(record)
          items.append(record)
        output_file.write("FOR doc in "+ json.dumps(items)+" INSERT doc IN skill_relevancies")
    except Exception as e:
        print(str(e))

if __name__ == '__main__':
  migration = Migration()
  # migration.migrate_transactions(5000)
  # migration.system_skills()
  migration.employee_skills()
  # migration.relevancies() 
  # migration.transactions(1000)

  # migration.migrate_skills_for_autocomplete(100)
  # migration.phuclq_system_skills()
  # migration.phuclq_skill_similarity_edges()


#create relationship between 1 node and n-1 node


# curl -s -H "Content-Type: application/x-ndjson" -XPOST "localhost:9200/_bulk" --data-binary @"system_skills.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"system_skills_with_relationships.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"employees.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"system_skills_autocomplete.json"

"""
collections:
  system_skills
  employee_skills
  relevancies
  transactions

"""