from lxml import html
import requests
import csv
import os


class ItViecCrawler:
    # https://itviec.com/it-jobs?page=5

    def __init__(self, depth):
        self.depth = depth
        self.skills = []
        self.pages = 70
        self.home_page = 'https://itviec.com/it-jobs'
        self.output_file_path = os.path.join(
            os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
            'crawler',
            'output',
            'technologies.csv'
        )
        self.writer = None

    def crawl(self):
        num = 1
        while num <= self.pages:
            url = f'{self.home_page}?page={num}'
            self.get_technologies_from_single_page(url)
            num += 1

    def get_technologies_from_single_page(self, url):
      try:
        with open(file=self.output_file_path, mode='a+', encoding='utf-8') as output_file:
          technology = None
          start_page = requests.get(url)
          tree = html.fromstring(start_page.text)
          technologies = tree.xpath('//div[@class="job-bottom"]/div[@class="tag-list"]/a/span/text()')
          for technology in technologies:
            technology = technology.strip("\n")
            print(technology)
            if technology in self.skills:
              pass
            else:
              self.skills.append(technology)
              output_file.write(technology+"\n")
        print(skills)
      except Exception as e:
        print(e)
        print(url)
        


crawler = ItViecCrawler(0)
crawler.crawl()