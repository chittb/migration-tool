import os
import csv
import random
import json
import names
import uuid

class Migration:
  
  def __init__(self):
    super().__init__()

  def get_skills(self, file_name):
    res = []
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', file_name) 
    with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            record = {
              "company": row[0],
              "skill_id": row[1],
              "skill_name": row[2],
              "parent_id": row[3],
              "category": row[4]
            }
            res.append(record)
    self.skills = res
    return res

  """ def apriori_transactions_hardskills(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'transactions_hardskills.csv')
      skills = self.get_skills('system_skills_hardskill.csv')
      with open(file=output_file_path, mode='w+', encoding='utf-8',newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        i = 0
        while(i<number):
          weight = int(round(random.uniform(1,7), 0))
          count = 0
          transaction = []
          while(count<weight):
            item = random.choice(skills)
            if item["skill_name"] not in transaction:
              transaction.append(item["skill_name"])
              count+=1
          print(transaction)
          csv_writer.writerow(transaction)
          i+=1
      csv_file.close()
      
    except Exception as e:
      print(str(e))

  def apriori_transactions_softskills(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'transactions_softskills.csv')
      skills = self.get_skills('system_skills_softskill.csv')
      with open(file=output_file_path, mode='w+', encoding='utf-8',newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        i = 0
        while(i<number):
          weight = int(round(random.uniform(1,7), 0))
          count = 0
          transaction = []
          while(count<weight):
            item = random.choice(skills)
            if item["skill_name"] not in transaction:
              transaction.append(item["skill_name"])
              count+=1
          print(transaction)
          csv_writer.writerow(transaction)
          i+=1
      csv_file.close()
      
    except Exception as e:
      print(str(e))
   """

  def apriori_transactions_hardskills(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'transactions_hardskills.csv')
      skills = self.get_skills('system_skills_hardskill.csv')
      with open(file=output_file_path, mode='w+', encoding='utf-8',newline='') as output_file:
        i = 0
        transaction = []
        while(i<number):
          weight = int(round(random.uniform(1,7), 0))
          count = 0
          items = []
          while(count<weight):
            item = random.choice(skills)
            if item["skill_name"] not in items:
              items.append(item["skill_name"])
              count+=1
          transaction.append(items)
          i+=1
        output_file.write(str(transaction))  
      
    except Exception as e:
      print(str(e))

  def apriori_transactions_softskills(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'transactions_softskills.csv')
      skills = self.get_skills('system_skills_softskill.csv')
      with open(file=output_file_path, mode='w+', encoding='utf-8',newline='') as output_file:
        i = 0
        transaction = []
        while(i<number):
          weight = int(round(random.uniform(1,7), 0))
          count = 0
          items = []
          while(count<weight):
            item = random.choice(skills)
            if item["skill_name"] not in items:
              items.append(item["skill_name"])
              count+=1
          transaction.append(items)
          i+=1
        output_file.write(str(transaction))  
      
    except Exception as e:
      print(str(e))
  
  def word2vec_transactions(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'transactions_softskills.csv')
      skills = self.get_skills()
      with open(file=output_file_path, mode='w+', encoding='utf-8',newline='') as csv_file:
        csv_writer = csv.writer(csv_file)
        header = ["group_id", "skill_id", "skill_name"]
        csv_writer.writerow(header)
        i = 0
        while(i<number):
          weight = int(round(random.uniform(3,5), 0))
          count = 0
          items = []
          group_id =  str(uuid.uuid4())
          while(count<weight):
            item = random.choice(skills)
            if item["skill_id"] not in items:
              items.append(item["skill_id"])
              transaction = [group_id, item["skill_id"], item["skill_name"]]
              csv_writer.writerow(transaction)
              print(transaction)
              count+=1
          i+=1
      csv_file.close()
      
    except Exception as e:
      print(str(e))


if __name__ == '__main__':
  migration = Migration()
  # migration.migrate_transactions_test_algorithms(1000)
  # migration.migrate_skills_for_autocomplete(100)
  migration.apriori_transactions_softskills(200)
  # migration.apriori_transactions_hardskills(2000)



#create relationship between 1 node and n-1 node


# curl -s -H "Content-Type: application/x-ndjson" -XPOST "localhost:9200/_bulk" --data-binary @"system_skills.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"system_skills_with_relationships.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"employees.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"system_skills_autocomplete.json"