import os
import csv
import random
import json
import names
import uuid


class ElasticsearchMigration:
  
  def __init__(self):
    super().__init__()

  def get_skills(self):
    res = []
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'dataset_system_skill.csv') 
    with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            record = {
              "skill_id": row[0],
              "skill_name": row[1]
            }
            res.append(record)
    return res

  def migrate_skills(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'system_skills.json') 
      skills = self.get_skills()
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        for skill in skills:
          index = { 
            "create" : { 
              "_index" : "skills", 
              }
            }
          record = {
              "skill_id": skill["skill_id"],
              "skill_name": skill["skill_name"]
          }
          output_file.write(json.dumps(index)+"\n")
          output_file.write(json.dumps(record)+"\n")
          print(record)   
      
    except Exception as e:
      print(str(e))

  def migrate_relationships(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'system_skills_with_relationships.json') 
      skills = self.get_skills()
      index = { 
        "create" : { 
          "_index" : "skill_relationships"
        }
      }
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        for skill in skills:
          self_skill = {
            "relationship_type": "relevant",
            "relationship_similarity": 1,
            "relationship_skill": {
              "skill_id": skill["skill_id"],
              "skill_name": skill["skill_name"]
            },
            "relationship_relevant_skill": {
              "skill_id": skill["skill_id"],
              "skill_name": skill["skill_name"]
            }
          }
          output_file.write(json.dumps(index)+"\n")
          output_file.write(json.dumps(self_skill)+"\n") 

        # n records
        i = 0
        while(i<number):
          weight = round(random.uniform(0,1), 2)
          skill1 = random.choice(skills)
          skill2 = random.choice(skills)
          if skill1["skill_id"] != skill2["skill_id"]:
            record = {
              "relationship_type": "relevant",
              "relationship_similarity": weight,
              "relationship_skill": {
                "skill_id": skill1["skill_id"],
                "skill_name": skill1["skill_name"]
              },
              "relationship_relevant_skill": {
                "skill_id": skill2["skill_id"],
                "skill_name": skill2["skill_name"]
              }
            }
            output_file.write(json.dumps(index)+"\n")
            output_file.write(json.dumps(record)+"\n")
            print(record)   
            i+=1
      
    except Exception as e:
      print(str(e))

  def migrate_employees(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'employees.json') 
      system_skills = self.get_skills()
      index = { 
        "create" : { 
          "_index" : "employees"
        }
      }
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        # n records
        i = 0
        while(i<number):
          skill_number = int(round(random.uniform(3,5), 0))
          num = 0
          employee_skills = []
          while(num<skill_number):
            skill = random.choice(system_skills)
            skill["skill_level"] = int(round(random.uniform(1,5), 0))
            employee_skills.append(skill)
            num+=1
          employee = {
            "employee_id": str(uuid.uuid4()),
            "employee_name": names.get_full_name(),
            "employee_skills": employee_skills
          }

          
          output_file.write(json.dumps(index)+"\n")
          output_file.write(json.dumps(employee)+"\n")
          print(employee)   
          i+=1
      
    except Exception as e:
      print(e)

  def migrate_skills_for_autocomplete(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'system_skills_autocomplete.json') 
      skills = self.get_skills()
      index = { 
        "create" : { 
          "_index" : "skill_autocomplete"
        }
      }
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        # n records
        i = 0
        while(i<number):
          skill_number = int(round(random.uniform(2,5), 2))
          num = 0
          skill_string = ''
          record = {}
          while(num<skill_number):
            skill = random.choice(skills)
            skill_string = '{skill_string} {skill_name}'.format(skill_string=skill_string, skill_name=skill["name"])
            num += 1
          record["skill_id"] = str(uuid.uuid4())
          record["skill_name"] = skill_string
          output_file.write(json.dumps(index)+"\n")
          output_file.write(json.dumps(record)+"\n")
          print(record)
          i += 1
      
    except Exception as e:
      print(str(e))

if __name__ == '__main__':
  migration = ElasticsearchMigration()
  migration.migrate_skills()
  migration.migrate_relationships(5000)
  migration.migrate_employees(100)
  # migration.migrate_skills_for_autocomplete(100)



#create relationship between 1 node and n-1 node


# curl -s -H "Content-Type: application/x-ndjson" -XPOST "localhost:9200/_bulk" --data-binary @"system_skills.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"system_skills_with_relationships.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"employees.json"
# curl -s -H "Content-Type: application/x-ndjson" -XPUT "localhost:9200/_bulk" --data-binary @"system_skills_autocomplete.json"