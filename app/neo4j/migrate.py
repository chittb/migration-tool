import os
import csv
import random

class Neo4jMigration:
  def __init__(self):
    super().__init__()

  def create_nodes(self, output_file_path, input_file_path):
    try:
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            cypher = """CREATE ({node_name}:{node_type} {title}"{node_title}", description: "{node_description}" """.format(node_name=row[0], node_title=row[1], title='{ title: ', node_type=row[2],node_description=row[3])+" })\n"
            output_file.write(cypher)
            print(cypher)
    except Exception as e:
      print(str(e))

  def create_relationships(self, output_file_path, input_file_path):
    try:
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            weight = round(random.uniform(0,1), 2)
            record1 = "CREATE ("+row[0]+")-[:similarity {weight:"+str(weight)+"}]->("+row[1]+")\n"
            record2 = "CREATE ("+row[1]+")-[:similarity {weight:"+str(1)+"}]->("+row[0]+")\n"
            output_file.write(record1)
            output_file.write(record2)
            print(record1)
            print(record2)
    except Exception as e:
      print(str(e))

  def migrate(self):
    try:
      create_nodes_output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'system-skills.cypher') 
      create_nodes_input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'dataset_system-skills_ver2.csv') 
      self.create_nodes(create_nodes_output_file_path, create_nodes_input_file_path)

      create_relationships_output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'system-skills.cypher') 
      create_relationships_input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'dataset_system-skills_relationships.csv')
      self.create_relationships(create_relationships_output_file_path, create_relationships_input_file_path)
      
    except Exception as e:
      print(str(e))
