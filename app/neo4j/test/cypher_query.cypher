MATCH (p:Word)
 WITH {item:id(p), weights: p.embedding} AS wordData
 WITH collect(wordData) AS data
 CALL gds.alpha.similarity.cosine.write({
   nodeProjection: '*',
   relationshipProjection: '*',
   data: data,
   // here is where you define how many nearest neighbours should be stored
   topK: 1,
   // here you define what is the minimal similarity between a 
   // given pair of node to be still relevant
   similarityCutoff: 0.1
 })
 YIELD nodes, similarityPairs, writeRelationshipType, writeProperty, min, max, mean, stdDev, p25, p50, p75, p90, p95, p99, p999, p100
 RETURN nodes, similarityPairs, writeRelationshipType, writeProperty, min, max, mean, p95

MATCH (w:Word)-[:SIMILAR]-(other)
RETURN other

MATCH (p)
 WITH {item:id(p), weights: p.embedding} AS wordData
 WITH collect(wordData) AS data
 CALL gds.alpha.similarity.cosine.write({
   nodeProjection: '*',
   relationshipProjection: '*',
   data: data,
   // here is where you define how many nearest neighbours should be stored
   topK: 1,
   // here you define what is the minimal similarity between a 
   // given pair of node to be still relevant
   similarityCutoff: 0.1
 })
 YIELD nodes, similarityPairs, writeRelationshipType, writeProperty, min, max, mean, stdDev, p25, p50, p75, p90, p95, p99, p999, p100
 RETURN nodes, similarityPairs, writeRelationshipType, writeProperty, min, max, mean, p95

MATCH (w:Word)-[:SIMILAR]-(other)
RETURN other
