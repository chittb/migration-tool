import os
import csv
import random
import json
import names
import uuid

class ElasticsearchMigration:
  
  def __init__(self):
    super().__init__()
    self.skills = self.get_skills()

  def get_skills(self):
    res = []
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'dataset_system_skill.csv') 
    with open(input_file_path, mode='r', encoding='utf-8') as csv_file:
          csv_reader = csv.reader(csv_file, delimiter=',')
          for row in csv_reader:
            record = {
              "skill_id": row[0],
              "skill_name": row[1]
            }
            res.append(record)
    return res

  def migrate_skills(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'script.sql') 
      with open(file=output_file_path, mode='a+', encoding='utf-8') as output_file:
        sql = "insert into skills (id,name) values"
        rows = ""
        for skill in self.skills:
          record = "('{id}', '{name}')".format(id=skill["skill_id"], name=skill["skill_name"])
          if rows == "":
            rows = record
          else:
            rows = "{rows},\n{record}".format(rows=rows, record=record)
        sql = "{sql}\n{rows};\n".format(sql=sql, rows=rows)
        output_file.write(sql) 
        print(sql) 
      
    except Exception as e:
      print(str(e))

  def migrate_relationships(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'script.sql')
      skills = self.get_skills()
      with open(file=output_file_path, mode='a+', encoding='utf-8') as output_file:
        sql = "insert into relationships (skill_id,relevant_skill_id,similarity) values"
        rows = ""
        for skill in skills:
          record = "('{skill_id}', '{relevant_skill_id}', '{similarity}')".format(skill_id=skill["skill_id"], relevant_skill_id=skill["skill_id"], similarity=1)
          if rows == "":
            rows = record
          else:
            rows = "{rows},\n{record}".format(rows=rows, record=record)
        i = 0
        while(i<number):
          skill = random.choice(skills)
          relevant_skill = random.choice(skills)
          similarity = round(random.uniform(0,1), 2)
          i+=1
          record = "('{skill_id}', '{relevant_skill_id}', '{similarity}')".format(skill_id=skill["skill_id"], relevant_skill_id=relevant_skill["skill_id"], similarity=similarity)
          rows = "{rows},\n{record}".format(rows=rows, record=record)
        sql = "{sql}\n{rows};\n".format(sql=sql, rows=rows)
        output_file.write(sql) 
        print(sql) 
    except Exception as e:
      print(str(e))


  def migrate_employees(self, number):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'script.sql') 
      with open(file=output_file_path, mode='a+', encoding='utf-8') as output_file:
        sql = "insert into employees (id,name) values"
        rows = ""
        i = 0
        employee_ids = []
        while(i<number):
          employee_id = str(uuid.uuid4())
          if employee_id not in employee_ids:
            employee_ids.append(employee_id)
            record = "('{id}', '{name}')".format(id=employee_id, name=names.get_full_name())
            self.migrate_employee_skills(employee_id)
            if rows == "":
              rows = record
            else:
              rows = "{rows},\n{record}".format(rows=rows, record=record)
            i+=1
        sql = "{sql}\n{rows};\n".format(sql=sql, rows=rows)
        output_file.write(sql) 
        print(sql) 
    except Exception as e:
      print(str(e))


  def migrate_employee_skills(self, employee_id):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'script.sql') 
      with open(file=output_file_path, mode='a+', encoding='utf-8') as output_file:
        sql = "insert into employee_skills (employee_id,skill_id,level) values"
        rows = ""
        number = int(round(random.uniform(3,5), 0))
        i = 0
        skills = []
        while(i<number):
          skill = random.choice(self.skills)
          if skill not in skills:
            skills.append(skill)
            level = int(round(random.uniform(1,5), 0))
            record = "('{employee_id}', '{skill_id}', '{level}')".format(employee_id=employee_id, skill_id=skill["skill_id"], level=level)
            if rows == "":
              rows = record
            else:
              rows = "{rows},\n{record}".format(rows=rows, record=record)
            i+=1
        sql = "{sql}\n{rows};\n".format(sql=sql, rows=rows)
        output_file.write(sql) 
        print(sql) 
    except Exception as e:
      print(str(e))

  
if __name__ == '__main__':
  migration = ElasticsearchMigration()
  migration.migrate_skills()
  migration.migrate_relationships(5000)
  migration.migrate_employees(100)