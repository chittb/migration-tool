drop table if exists skills;
drop table if exists relationships;
drop table if exists employees;
drop table if exists employee_skills;

create table skills (
    id character varying(50) not null
  , name character varying(255) not null
  , active_flag boolean default true not null
  , constraint skill_matching_skill_PKC primary key (id)
) ;

create table relationships (
    id serial
  , skill_id character varying(50) not null
  , relevant_skill_id character varying(50) not null
  , similarity numeric not null default 0
  , constraint skill_matching_relationship_PKC primary key (id)
) ;

create table employees (
    id character varying(50) not null
  , name character varying(255) not null
  , active_flag boolean default true not null
  , constraint skill_matching_employee_PKC primary key (id)
) ;

create table employee_skills (
    employee_id character varying(50) not null
  , skill_id character varying(50) not null
  , level numeric
  , constraint skill_matching_employee_skills_PKC primary key(employee_id, skill_id)
)