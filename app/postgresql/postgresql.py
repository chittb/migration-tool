import os
import json
import random

class Postgresql:
  def __init__(self):
    super().__init__()

  def get_employees(self):
    input_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'input', 'employees.json') 
    with open(input_file_path, 'r', encoding='cp932', errors='ignore') as f:
      data = json.loads(f.read())
    return data["data"]

  def migrate_employee_skills(self):
    try:
      output_file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output', 'employee_skills.sql') 
      employees = self.get_employees()
      records = []
      with open(file=output_file_path, mode='w+', encoding='utf-8') as output_file:
        sql = "insert into employee.employee_skill (company,emp_id,skill_id,sort_no,level,experienced_date,used_date,change_count,create_by,create_at,change_by,change_at) values"
        records = ""
        for employee in employees:
          employee_skills = employee["employee_skills"]
          i = 0
          while i<len(employee_skills):
            level = int(round(random.uniform(3,5), 0))
            experienced_date = int(round(random.uniform(0,5), 0))
            record = "('{company}', '{emp_id}', '{skill_id}', '{sort_no}', '{level}', '{experienced_date}', '{used_date}', '{change_count}', '{create_by}', '{create_at}', '{change_by}', '{change_at}')".format(
              company='ait', 
              emp_id=employee["id"], 
              skill_id=employee_skills[i]["skill_id"], 
              sort_no=i, 
              level=level, 
              experienced_date=experienced_date, 
              used_date='8/5/2020', 
              change_count=1, 
              create_by='46dd1d16-3d16-40b0-9294-9992868c5059', 
              create_at='8/7/2020 4:39:11 AM.156', 
              change_by='46dd1d16-3d16-40b0-9294-9992868c5059', 
              change_at='8/7/2020 4:39:11 AM.156')
            if records == "":
              records = record
            else:
              records = "{records},\n{record}".format(records=records, record=record)
            i+=1
        sql = "{sql}\n{records};\n".format(sql=sql, records=records)
        output_file.write(sql) 
    except Exception as e:
      print(e)

  
if __name__ == '__main__':
  postgresql = Postgresql()
  postgresql.migrate_employee_skills()